<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class AppController extends Controller
{
    public function index(Request $request)
    {
        $data['title'] = "Dashboard";
        $data['subtitle'] = "Hi, ".auth()->user()->name;
        return view('home')->with($data);
    }
}
